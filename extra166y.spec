Name:	        extra166y
Version:	    1.7.0
Release:	    11
Summary:	    A collection that supports parallel operations
License:	    Public Domain
URL:		    http://gee.cs.oswego.edu/dl/concurrency-interest
Source0:	    jsr166-%{version}.tar.xz
Source1:        http://repository.codehaus.org/org/codehaus/jsr166-mirror/%{name}/%{version}/%{name}-%{version}.pom
Source2:        extra166y-OSGi.bnd

Patch0:         extra166y-osgi-manifest.patch

BuildRequires:	ant aqute-bnd >= 3.2.0-2 javapackages-local junit

BuildArch:      noarch

Provides: 	extra166y-javadoc = %{version}-%{release}
Obsoletes: 	extra166y-javadoc < %{version}-%{release}

%description
A collection that supports parallel operations

%package_help

%prep
%autosetup -n jsr166 -p0

for java_file in $(find . -name "*.java");
do
  sed -i "s|jsr166y.|java.util.concurrent.|" ${java_file}
done

sed -i '/configure-compiler, jsr166ycompile/d' build.xml
sed -i '/<compilerarg line="${build.args}"/d' build.xml

install -p %{SOURCE2} extra166y.bnd
sed -i "s|@VERSION@|%{version}|" extra166y.bnd

%build

%{_bindir}/python3 %{_datadir}/java-utils/mvn_file.py org.codehaus.jsr166-mirror:%{name} %{name}
export CLASSPATH=%{_datadir}/java/junit.jar
ant extra166yjar extra166ydist-docs
%{_bindir}/python3 %{_datadir}/java-utils/mvn_artifact.py %{SOURCE1} build/%{name}lib/%{name}.jar

%install
%mvn_install -J dist/%{name}docs


%files
%defattr(-,root,root)
%doc src/main/intro.html
%{_datadir}/java/%{name}.jar
%{_datadir}/maven-poms/%{name}.pom
%{_datadir}/maven-metadata/%{name}.xml
%{_datadir}/javadoc/%{name}/*.html
%{_datadir}/javadoc/%{name}/stylesheet.css
%{_datadir}/javadoc/%{name}/script.js
%{_datadir}/javadoc/%{name}/package-list
%{_datadir}/javadoc/%{name}/%{name}/*.html


%files     help
%defattr(-,root,root)
%doc src/main/readme 


%changelog
* Mon Dec 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.7.0-11
- Package init
